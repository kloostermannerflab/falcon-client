import time

import zmq

port = 5556

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

for k in range(10):
    socket.send("topic message %d" % k)
    time.sleep(1)
