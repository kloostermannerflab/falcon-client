import argparse
import datetime
import enum
import multiprocessing
import re
import sys

import yaml
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QThread
from PyQt5.QtGui import QBrush
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QInputDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QStyledItemDelegate
from PyQt5.QtWidgets import QTreeWidgetItem

from falcon_clients.falcon_server import Falcon
from falcon_clients.falcon_server import FalconError
from falcon_clients.falcon_server import FalconEvents
from falcon_clients.server_control.ui.simple_client.custom_graph import Ui_Dialog
from falcon_clients.server_control.ui.simple_client.falcon_client_design import (
    Ui_MainWindow,
)


class CustomGraphDialog(QDialog):
    def __init__(self, **kwargs):
        QDialog.__init__(self, **kwargs)

        # 1. set up the user interface from Designer
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

    def load_graph(self):
        # ask for file
        d = str(
            QFileDialog.getOpenFileName(
                parent=self, caption="Open", filter="Graph definition (*.yaml)"
            )[0]
        )

        if len(d) > 0:
            with open(d, "r") as fid:
                # g = yaml.load( fid )
                g = fid.read()

            self.ui.plainTextEdit.setPlainText(g)

    def save_graph(self):
        # ask for file
        d = str(
            QFileDialog.getSaveFileName(
                parent=self, caption="Save as", filter="Graph definition (*.yaml)"
            )[0]
        )

        if len(d) > 0:

            with open(d, "w") as fid:
                fid.write(self.ui.plainTextEdit.toPlainText())

        # QMessageBox.warning(self, "Error", "Error saving graph definition: " + e.message )


def _local_match(target, expr, use_regex=False):

    if use_regex:
        return re.search(expr, target) is not None
    else:
        return expr in target


GraphInfoType = enum.Enum(
    "GraphInfoType", "inputs outputs states methods options", start=1001
)


def dict2subtree(d, parent=None, type=0, editable=False):

    can_edit = editable

    for k, v in d.items():

        if isinstance(editable, dict):
            can_edit = editable.get(k, False)

        if isinstance(v, dict):
            parent = QTreeWidgetItem(parent, [k, ""], type)
            dict2subtree(v, parent, type, can_edit)
        else:
            item = QTreeWidgetItem(parent, [k, str(v)], type)
            if can_edit:
                item.setFlags(item.flags() | Qt.ItemIsEditable)
                item.setForeground(0, QBrush(Qt.darkGreen))
                item.setForeground(1, QBrush(Qt.darkGreen))


class NoEditDelegate(QStyledItemDelegate):
    def createEditor(self, *args):
        return None


class FalconEventThread(QThread):

    event = pyqtSignal(dict)

    def __init__(self, address="localhost", port=5556):
        QThread.__init__(self)
        self._falcon = FalconEvents(address=address, port=port)
        self._done = False

    def __del__(self):
        self.stop()

    def stop(self):
        self._done = True
        self.wait()

    def run(self):
        while not self._done:
            while self._falcon.poll():
                event = self._falcon.parse_event(self._falcon.get_next_event())
                self.event.emit(event)
            self.msleep(10)
        self._done = True


class FalconClientWindow(QMainWindow):
    def __init__(self, address="localhost", port=5555, logport=5556):
        QMainWindow.__init__(self)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.graphTree.setItemDelegateForColumn(0, NoEditDelegate())

        self._address = str(address)
        self._port = int(port)
        self._logport = int(logport)

        self.thread = FalconEventThread(address=self._address, port=self._logport)
        self.falcon = Falcon(address=self._address, port=self._port)

        self._graph_filter = [
            GraphInfoType["inputs"],
            GraphInfoType["outputs"],
            GraphInfoType["options"],
            GraphInfoType["states"],
            GraphInfoType["methods"],
        ]
        self._graph_filter_ui_set()

        self._events = []

        self._event_filter_type = ["info", "state", "update", "error"]

        self._event_filter_what_enabled = False
        self._event_filter_what = ""
        self._event_filter_what_regex = False

        now = datetime.datetime.now()
        now = now - datetime.timedelta(microseconds=now.microsecond)

        self._event_filter_when_from_enabled = False
        self._event_filter_when_from = now

        self._event_filter_when_to_enabled = False
        self._event_filter_when_to = now

        self._event_filter_where_enabled = False
        self._event_filter_where = ""
        self._event_filter_where_regex = False

        self._event_filter_ui_set()

        self.ui.revertFilters.setEnabled(False)
        self.ui.applyFilters.setEnabled(False)

        self._event_default_format = "{when:%H:%M:%S} {kind} {what}"
        self._event_format = dict(
            debug='<font color="Black"><b>{when:%H:%M:%S}</b> {what}</font> <font color="ForestGreen"><i>{where}</i></font>',
            info='<font color="RoyalBlue"><b>{when:%H:%M:%S}</b> {kind} - {what}</font>',
            update='<font color="OrangeRed"><b>{when:%H:%M:%S}</b> {kind} - {what}</font>',
        )

        self.thread.event.connect(self.process_event)

        self.thread.start()

    def _graph_filter_ui_set(self):

        self.ui.graph_inputs_filter.blockSignals(True)
        self.ui.graph_outputs_filter.blockSignals(True)
        self.ui.graph_options_filter.blockSignals(True)
        self.ui.graph_states_filter.blockSignals(True)
        self.ui.graph_methods_filter.blockSignals(True)

        self.ui.graph_inputs_filter.setChecked(
            GraphInfoType["inputs"] in self._graph_filter
        )
        self.ui.graph_outputs_filter.setChecked(
            GraphInfoType["outputs"] in self._graph_filter
        )
        self.ui.graph_options_filter.setChecked(
            GraphInfoType["options"] in self._graph_filter
        )
        self.ui.graph_states_filter.setChecked(
            GraphInfoType["states"] in self._graph_filter
        )
        self.ui.graph_methods_filter.setChecked(
            GraphInfoType["methods"] in self._graph_filter
        )

        self.ui.graph_inputs_filter.blockSignals(False)
        self.ui.graph_outputs_filter.blockSignals(False)
        self.ui.graph_options_filter.blockSignals(False)
        self.ui.graph_states_filter.blockSignals(False)
        self.ui.graph_methods_filter.blockSignals(False)

    def _graph_filter_ui_get(self):

        f = []
        if self.ui.graph_inputs_filter.isChecked():
            f.append(GraphInfoType["inputs"])
        if self.ui.graph_outputs_filter.isChecked():
            f.append(GraphInfoType["outputs"])
        if self.ui.graph_options_filter.isChecked():
            f.append(GraphInfoType["options"])
        if self.ui.graph_states_filter.isChecked():
            f.append(GraphInfoType["states"])
        if self.ui.graph_methods_filter.isChecked():
            f.append(GraphInfoType["methods"])
        self._graph_filter = f

    def _event_filter_ui_set(self):

        self.ui.info_filter.setChecked("info" in self._event_filter_type)
        self.ui.state_filter.setChecked("state" in self._event_filter_type)
        self.ui.update_filter.setChecked("update" in self._event_filter_type)
        self.ui.event_filter.setChecked("event" in self._event_filter_type)
        self.ui.debug_filter.setChecked("debug" in self._event_filter_type)
        self.ui.error_filter.setChecked("error" in self._event_filter_type)

        self.ui.what_filter.setChecked(self._event_filter_what_enabled)
        self.ui.when_from_filter.setChecked(self._event_filter_when_from_enabled)
        self.ui.when_to_filter.setChecked(self._event_filter_when_to_enabled)
        self.ui.where_filter.setChecked(self._event_filter_where_enabled)

        self.ui.what.setText(self._event_filter_what)
        self.ui.where.setText(self._event_filter_where)

        self.ui.when_from_date.setDate(self._event_filter_when_from.date())
        self.ui.when_to_date.setDate(self._event_filter_when_to.date())

        self.ui.when_from_time.setTime(self._event_filter_when_from.time())
        self.ui.when_to_time.setTime(self._event_filter_when_to.time())

        self.ui.what_regex.setChecked(self._event_filter_what_regex)
        self.ui.where_regex.setChecked(self._event_filter_where_regex)

    def _event_filter_ui_get(self):

        f = []
        if self.ui.info_filter.isChecked():
            f.append("info")
        if self.ui.state_filter.isChecked():
            f.append("state")
        if self.ui.update_filter.isChecked():
            f.append("update")
        if self.ui.event_filter.isChecked():
            f.append("event")
        if self.ui.debug_filter.isChecked():
            f.append("debug")
        if self.ui.error_filter.isChecked():
            f.extend(["warning", "error"])

        self._event_filter_type = f

        self._event_filter_what_enabled = self.ui.what_filter.isChecked()
        self._event_filter_when_from_enabled = self.ui.when_from_filter.isChecked()
        self._event_filter_when_to_enabled = self.ui.when_to_filter.isChecked()
        self._event_filter_where_enabled = self.ui.where_filter.isChecked()

        self._event_filter_what = str(self.ui.what.text())
        self._event_filter_where = str(self.ui.where.text())

        self._event_filter_when_from = datetime.datetime.combine(
            self.ui.when_from_date.date().toPyDate(),
            self.ui.when_from_time.time().toPyTime(),
        )
        self._event_filter_when_to = datetime.datetime.combine(
            self.ui.when_to_date.date().toPyDate(),
            self.ui.when_to_time.time().toPyTime(),
        )

        self._event_filter_what_regex = self.ui.what_regex.isChecked()
        self._event_filter_where_regex = self.ui.where_regex.isChecked()

    @pyqtSlot(QTreeWidgetItem, int)
    def state_changed(self, item, col):

        processor = item.parent().parent().text(0)
        state = item.text(0)
        value = item.text(col)

        try:
            b = self.falcon.update_state(processor, state, value)
            if not b:
                item.setText(1, self.falcon.retrieve_state(processor, state))
        except:
            # no connection
            QMessageBox.warning(
                self, "Error", "Error updating processor state. Graph data is invalid."
            )
            self.ui.graphTree.clear()

    @pyqtSlot()
    def graph_filter_changed(self):

        self._graph_filter_ui_get()

        for k in range(self.ui.graphTree.topLevelItemCount()):

            parent = self.ui.graphTree.topLevelItem(k)

            for c in range(parent.childCount()):

                child = parent.child(c)

                child.setHidden(GraphInfoType(child.type()) not in self._graph_filter)

    @pyqtSlot()
    def refresh_graph(self):
        self.ui.graphTree.clear()
        try:
            g = self.falcon.graph_definition()
        except:
            QMessageBox.warning(
                self,
                "Error",
                "Could not get graph definition. Falcon server may be unavailable.",
            )
            return

        if g is None or len(g) == 0:
            return

        if "graph" in g: 
            processors = g["graph"]["processors"]
        else:
            processors = g["processors"]
            
        for p, data in processors.items():
            top_item = QTreeWidgetItem(None, [p, data["class"]])

            if "inports" in data:
                child_item = QTreeWidgetItem(
                    top_item, ["input ports", ""], GraphInfoType["inputs"].value
                )
                dict2subtree(data["inports"], child_item, GraphInfoType["inputs"].value)

            if "outports" in data:
                child_item = QTreeWidgetItem(
                    top_item, ["output ports", ""], GraphInfoType["outputs"].value
                )
                dict2subtree(
                    data["outports"], child_item, GraphInfoType["outputs"].value
                )

            if "states" in data:
                state_data = {k: v["value"] for k, v in data["states"].items() if v["permission"] != "N"}
                editable = {
                    k: v["permission"] == "W" for k, v in data["states"].items()
                }
                states_item = QTreeWidgetItem(
                    top_item, ["states", ""], GraphInfoType["states"].value
                )
                dict2subtree(
                    state_data, states_item, GraphInfoType["states"].value, editable
                )

            if "methods" in data:
                dict2subtree(
                    {"methods": data["methods"]},
                    top_item,
                    GraphInfoType["methods"].value,
                )

            if "options" in data:
                child_item = QTreeWidgetItem(
                    top_item, ["options", ""], GraphInfoType["options"].value
                )
                dict2subtree(
                    data["options"], child_item, GraphInfoType["options"].value
                )

            self.ui.graphTree.addTopLevelItem(top_item)
            top_item.setExpanded(True)

            if "states" in data:
                states_item.setExpanded(True)

        self.graph_filter_changed()

    def destroy_graph(self):
        try:
            self.falcon.graph_destroy()
        except FalconError as e:
            QMessageBox.warning(self, "Error", "Error destroying graph: " + str(e))
        finally:
            self.refresh_graph()

    def upload_graph(self):
        dlg = CustomGraphDialog(parent=self)
        ok = dlg.exec_()

        if not bool(ok):
            return

        g = str(dlg.ui.plainTextEdit.toPlainText())
        g = yaml.load(g, Loader=yaml.FullLoader)

        try:
            self.falcon.graph_build(g, replace=True)
        except FalconError as e:
            QMessageBox.warning(self, "Error", "Error uploading graph: " + str(e))
        finally:
            self.refresh_graph()

    def build_remote_graph(self):

        # ask for remote file name
        g, ok = QInputDialog.getText(self, "Remote graph definition", "path")

        if bool(ok):
            try:
                self.falcon.graph_buildfile(g, replace=True)
            except FalconError as e:
                QMessageBox.warning(self, "Error", "Error uploading graph: " + str(e))
            finally:
                self.refresh_graph()

    @pyqtSlot()
    def filter_changed(self):
        self.ui.revertFilters.setEnabled(True)
        self.ui.applyFilters.setEnabled(True)

    @pyqtSlot()
    def revert_filters(self):
        self._event_filter_ui_set()
        self.ui.revertFilters.setEnabled(False)
        self.ui.applyFilters.setEnabled(False)

    @pyqtSlot()
    def apply_filters(self):
        self._event_filter_ui_get()
        self.ui.applyFilters.setEnabled(False)
        self.ui.revertFilters.setEnabled(False)

        # filter/show all messages
        self.ui.messages.clear()
        for e in self._events:
            if self.filter_event(e):
                self.show_event(e)

    @pyqtSlot()
    def clear_messages(self):
        self._events = []
        self.ui.messages.clear()

    @pyqtSlot(dict)
    def process_event(self, event):

        # save event
        self._events.append(event)

        # apply filter and show
        if self.filter_event(event):
            self.show_event(event)

    def filter_event(self, event):
        # keep messages for which:
        # kind in type_filter
        # and when>=from
        # and when<=to
        # and what matches or where matches

        b = event["kind"] in self._event_filter_type

        if self._event_filter_when_from_enabled:
            b = b and event["when"] >= self._event_filter_when_from
        if self._event_filter_when_to_enabled:
            b = b and event["when"] <= self._event_filter_when_to

        m = not (self._event_filter_what_enabled or self._event_filter_where_enabled)

        m = m or (
            self._event_filter_what_enabled
            and _local_match(
                event["what"], self._event_filter_what, self._event_filter_what_regex
            )
        )
        m = m or (
            self._event_filter_where_enabled
            and _local_match(
                event["where"], self._event_filter_where, self._event_filter_where_regex
            )
        )

        return b and m

    def show_event(self, event):

        txt = self._event_format.get(event["kind"], self._event_default_format)

        # cursor = self.ui.messages.textCursor()
        # cursor.setPosition(0)
        # cursor.insertHtml( txt.format(**event) )
        self.ui.messages.append(txt.format(**event))

    def start_processing(self):
        self.ui.ServerControlGroup.setEnabled(False)
        try:
            self.falcon.graph_start()
        except:
            QMessageBox.warning(self, "Error", "Error starting graph processing")
        self.ui.ServerControlGroup.setEnabled(True)

    def stop_processing(self):
        self.ui.ServerControlGroup.setEnabled(False)
        try:
            self.falcon.graph_stop()
        except:
            QMessageBox.warning(self, "Error", "Error stopping graph processing")
        self.ui.ServerControlGroup.setEnabled(True)

    def quit_falcon_server(self):
        self.ui.ServerControlGroup.setEnabled(False)
        try:
            self.falcon.server_quit()
        except:
            QMessageBox.warning(self, "Error", "Error quitting falcon server")
        self.ui.ServerControlGroup.setEnabled(True)


def _run_ui(mainwindow, *args, **kwargs):
    app = QApplication(sys.argv)
    app.setStyle("cleanlooks")

    window = mainwindow(*args, **kwargs)
    window.show()

    ret = app.exec_()

    try:
        ret = window.__retrieve_result__()
    except AttributeError:
        pass

    return ret


class UiExecutor:
    def __init__(self, ui):
        self._ui = ui

    def __call__(self, *args, **kwargs):
        return self.run_sync(*args, **kwargs)

    def run_sync(self, *args, **kwargs):
        return _run_ui(self._ui, *args, **kwargs)

    def run_async(self, *args, **kwargs):
        pool = multiprocessing.Pool(processes=1)
        return pool.apply_async(
            _run_ui, args=(self._ui,) + args, kwds=kwargs, callback=None
        )


CLIENT = UiExecutor(FalconClientWindow)


def main():
    parser = argparse.ArgumentParser(
        description="Simple Falcon client",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("--ip", default="localhost", help="Falcon server ip-address")
    parser.add_argument("--port", default=5555, help="Falcon server command port")
    parser.add_argument("--log", default=5556, help="Falcon server cloud logging port")

    args = parser.parse_args()

    sys.exit(CLIENT(address=args.ip, port=args.port, logport=args.log))


if __name__ == "__main__":
    main()
