# -*- coding: utf-8 -*-
# Form implementation generated from reading ui file 'falcon_client_design.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1000, 700)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.splitter = QtWidgets.QSplitter(self.centralwidget)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.widget = QtWidgets.QWidget(self.splitter)
        self.widget.setObjectName("widget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.ServerControlGroup = QtWidgets.QGroupBox(self.widget)
        self.ServerControlGroup.setObjectName("ServerControlGroup")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.ServerControlGroup)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.startButton = QtWidgets.QPushButton(self.ServerControlGroup)
        self.startButton.setObjectName("startButton")
        self.horizontalLayout.addWidget(self.startButton)
        self.stopButton = QtWidgets.QPushButton(self.ServerControlGroup)
        self.stopButton.setObjectName("stopButton")
        self.horizontalLayout.addWidget(self.stopButton)
        self.quitButton = QtWidgets.QPushButton(self.ServerControlGroup)
        self.quitButton.setObjectName("quitButton")
        self.horizontalLayout.addWidget(self.quitButton)
        spacerItem = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout_2.addWidget(self.ServerControlGroup)
        self.MessagesGroup = QtWidgets.QGroupBox(self.widget)
        self.MessagesGroup.setObjectName("MessagesGroup")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.MessagesGroup)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget_5 = QtWidgets.QWidget(self.MessagesGroup)
        self.widget_5.setObjectName("widget_5")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget_5)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.widget_3 = QtWidgets.QWidget(self.widget_5)
        self.widget_3.setObjectName("widget_3")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.widget_3)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.FilterByTypeGroup = QtWidgets.QGroupBox(self.widget_3)
        self.FilterByTypeGroup.setObjectName("FilterByTypeGroup")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.FilterByTypeGroup)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.info_filter = QtWidgets.QCheckBox(self.FilterByTypeGroup)
        self.info_filter.setObjectName("info_filter")
        self.gridLayout_2.addWidget(self.info_filter, 0, 0, 1, 1)
        self.error_filter = QtWidgets.QCheckBox(self.FilterByTypeGroup)
        self.error_filter.setObjectName("error_filter")
        self.gridLayout_2.addWidget(self.error_filter, 1, 2, 1, 1)
        self.debug_filter = QtWidgets.QCheckBox(self.FilterByTypeGroup)
        self.debug_filter.setObjectName("debug_filter")
        self.gridLayout_2.addWidget(self.debug_filter, 1, 1, 1, 1)
        self.state_filter = QtWidgets.QCheckBox(self.FilterByTypeGroup)
        self.state_filter.setObjectName("state_filter")
        self.gridLayout_2.addWidget(self.state_filter, 0, 1, 1, 1)
        self.event_filter = QtWidgets.QCheckBox(self.FilterByTypeGroup)
        self.event_filter.setObjectName("event_filter")
        self.gridLayout_2.addWidget(self.event_filter, 1, 0, 1, 1)
        self.update_filter = QtWidgets.QCheckBox(self.FilterByTypeGroup)
        self.update_filter.setObjectName("update_filter")
        self.gridLayout_2.addWidget(self.update_filter, 0, 2, 1, 1)
        self.verticalLayout_4.addWidget(self.FilterByTypeGroup)
        self.FilterByPropertyGroup = QtWidgets.QGroupBox(self.widget_3)
        self.FilterByPropertyGroup.setObjectName("FilterByPropertyGroup")
        self.gridLayout = QtWidgets.QGridLayout(self.FilterByPropertyGroup)
        self.gridLayout.setObjectName("gridLayout")
        self.where_filter = QtWidgets.QCheckBox(self.FilterByPropertyGroup)
        self.where_filter.setObjectName("where_filter")
        self.gridLayout.addWidget(self.where_filter, 6, 0, 1, 1)
        self.when_from_filter = QtWidgets.QCheckBox(self.FilterByPropertyGroup)
        self.when_from_filter.setObjectName("when_from_filter")
        self.gridLayout.addWidget(self.when_from_filter, 3, 0, 1, 1)
        self.where = QtWidgets.QLineEdit(self.FilterByPropertyGroup)
        self.where.setObjectName("where")
        self.gridLayout.addWidget(self.where, 6, 1, 1, 1)
        self.what = QtWidgets.QLineEdit(self.FilterByPropertyGroup)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.what.sizePolicy().hasHeightForWidth())
        self.what.setSizePolicy(sizePolicy)
        self.what.setMinimumSize(QtCore.QSize(0, 0))
        self.what.setObjectName("what")
        self.gridLayout.addWidget(self.what, 1, 1, 1, 1)
        self.what_filter = QtWidgets.QCheckBox(self.FilterByPropertyGroup)
        self.what_filter.setObjectName("what_filter")
        self.gridLayout.addWidget(self.what_filter, 1, 0, 1, 1)
        self.when_to_filter = QtWidgets.QCheckBox(self.FilterByPropertyGroup)
        self.when_to_filter.setObjectName("when_to_filter")
        self.gridLayout.addWidget(self.when_to_filter, 4, 0, 1, 1)
        self.when_from_time = QtWidgets.QTimeEdit(self.FilterByPropertyGroup)
        self.when_from_time.setObjectName("when_from_time")
        self.gridLayout.addWidget(self.when_from_time, 3, 2, 1, 1)
        self.when_from_date = QtWidgets.QDateEdit(self.FilterByPropertyGroup)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.when_from_date.sizePolicy().hasHeightForWidth()
        )
        self.when_from_date.setSizePolicy(sizePolicy)
        self.when_from_date.setObjectName("when_from_date")
        self.gridLayout.addWidget(self.when_from_date, 3, 1, 1, 1)
        self.when_to_time = QtWidgets.QTimeEdit(self.FilterByPropertyGroup)
        self.when_to_time.setObjectName("when_to_time")
        self.gridLayout.addWidget(self.when_to_time, 4, 2, 1, 1)
        self.when_to_date = QtWidgets.QDateEdit(self.FilterByPropertyGroup)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.when_to_date.sizePolicy().hasHeightForWidth())
        self.when_to_date.setSizePolicy(sizePolicy)
        self.when_to_date.setObjectName("when_to_date")
        self.gridLayout.addWidget(self.when_to_date, 4, 1, 1, 1)
        self.what_regex = QtWidgets.QCheckBox(self.FilterByPropertyGroup)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.what_regex.sizePolicy().hasHeightForWidth())
        self.what_regex.setSizePolicy(sizePolicy)
        self.what_regex.setObjectName("what_regex")
        self.gridLayout.addWidget(self.what_regex, 1, 2, 1, 1)
        self.where_regex = QtWidgets.QCheckBox(self.FilterByPropertyGroup)
        self.where_regex.setObjectName("where_regex")
        self.gridLayout.addWidget(self.where_regex, 6, 2, 1, 1)
        self.verticalLayout_4.addWidget(self.FilterByPropertyGroup)
        self.horizontalLayout_2.addWidget(self.widget_3)
        self.widget_4 = QtWidgets.QWidget(self.widget_5)
        self.widget_4.setObjectName("widget_4")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.widget_4)
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.applyFilters = QtWidgets.QPushButton(self.widget_4)
        self.applyFilters.setObjectName("applyFilters")
        self.verticalLayout_5.addWidget(self.applyFilters)
        self.revertFilters = QtWidgets.QPushButton(self.widget_4)
        self.revertFilters.setObjectName("revertFilters")
        self.verticalLayout_5.addWidget(self.revertFilters)
        spacerItem1 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding
        )
        self.verticalLayout_5.addItem(spacerItem1)
        self.clearMessages = QtWidgets.QPushButton(self.widget_4)
        self.clearMessages.setObjectName("clearMessages")
        self.verticalLayout_5.addWidget(self.clearMessages)
        self.horizontalLayout_2.addWidget(self.widget_4)
        self.verticalLayout.addWidget(self.widget_5)
        self.messages = QtWidgets.QTextEdit(self.MessagesGroup)
        self.messages.setDocumentTitle("")
        self.messages.setReadOnly(True)
        self.messages.setObjectName("messages")
        self.verticalLayout.addWidget(self.messages)
        self.messages.raise_()
        self.widget_5.raise_()
        self.verticalLayout_2.addWidget(self.MessagesGroup)
        self.GraphGroup = QtWidgets.QGroupBox(self.splitter)
        self.GraphGroup.setObjectName("GraphGroup")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.GraphGroup)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.widget_2 = QtWidgets.QWidget(self.GraphGroup)
        self.widget_2.setObjectName("widget_2")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.widget_2)
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.buildRemoteGraph = QtWidgets.QPushButton(self.widget_2)
        self.buildRemoteGraph.setObjectName("buildRemoteGraph")
        self.gridLayout_4.addWidget(self.buildRemoteGraph, 0, 1, 1, 1)
        self.uploadGraph = QtWidgets.QPushButton(self.widget_2)
        self.uploadGraph.setObjectName("uploadGraph")
        self.gridLayout_4.addWidget(self.uploadGraph, 0, 2, 1, 1)
        self.destroyGraph = QtWidgets.QPushButton(self.widget_2)
        self.destroyGraph.setObjectName("destroyGraph")
        self.gridLayout_4.addWidget(self.destroyGraph, 1, 1, 1, 1)
        self.refreshGraph = QtWidgets.QPushButton(self.widget_2)
        self.refreshGraph.setObjectName("refreshGraph")
        self.gridLayout_4.addWidget(self.refreshGraph, 1, 2, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.gridLayout_4.addItem(spacerItem2, 0, 0, 1, 1)
        self.verticalLayout_3.addWidget(self.widget_2)
        self.groupBox = QtWidgets.QGroupBox(self.GraphGroup)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.graph_options_filter = QtWidgets.QCheckBox(self.groupBox)
        self.graph_options_filter.setObjectName("graph_options_filter")
        self.gridLayout_3.addWidget(self.graph_options_filter, 0, 2, 1, 1)
        self.graph_outputs_filter = QtWidgets.QCheckBox(self.groupBox)
        self.graph_outputs_filter.setObjectName("graph_outputs_filter")
        self.gridLayout_3.addWidget(self.graph_outputs_filter, 0, 1, 1, 1)
        self.graph_inputs_filter = QtWidgets.QCheckBox(self.groupBox)
        self.graph_inputs_filter.setObjectName("graph_inputs_filter")
        self.gridLayout_3.addWidget(self.graph_inputs_filter, 0, 0, 1, 1)
        self.graph_states_filter = QtWidgets.QCheckBox(self.groupBox)
        self.graph_states_filter.setObjectName("graph_states_filter")
        self.gridLayout_3.addWidget(self.graph_states_filter, 1, 0, 1, 1)
        self.graph_methods_filter = QtWidgets.QCheckBox(self.groupBox)
        self.graph_methods_filter.setObjectName("graph_methods_filter")
        self.gridLayout_3.addWidget(self.graph_methods_filter, 1, 1, 1, 1)
        self.verticalLayout_3.addWidget(self.groupBox)
        self.graphTree = QtWidgets.QTreeWidget(self.GraphGroup)
        self.graphTree.setIndentation(10)
        self.graphTree.setUniformRowHeights(True)
        self.graphTree.setAllColumnsShowFocus(False)
        self.graphTree.setObjectName("graphTree")
        self.graphTree.header().setVisible(True)
        self.graphTree.header().setCascadingSectionResizes(True)
        self.graphTree.header().setDefaultSectionSize(180)
        self.graphTree.header().setHighlightSections(False)
        self.graphTree.header().setMinimumSectionSize(50)
        self.graphTree.header().setSortIndicatorShown(True)
        self.graphTree.header().setStretchLastSection(True)
        self.verticalLayout_3.addWidget(self.graphTree)
        self.horizontalLayout_3.addWidget(self.splitter)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1000, 20))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.startButton.clicked.connect(MainWindow.start_processing)
        self.quitButton.clicked.connect(MainWindow.quit_falcon_server)
        self.stopButton.clicked.connect(MainWindow.stop_processing)
        self.revertFilters.clicked.connect(MainWindow.revert_filters)
        self.applyFilters.clicked.connect(MainWindow.apply_filters)
        self.clearMessages.clicked.connect(MainWindow.clear_messages)
        self.info_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.what_regex.stateChanged["int"].connect(MainWindow.filter_changed)
        self.when_to_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.when_from_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.where_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.what_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.error_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.debug_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.event_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.update_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.state_filter.stateChanged["int"].connect(MainWindow.filter_changed)
        self.where_regex.stateChanged["int"].connect(MainWindow.filter_changed)
        self.where.textChanged["QString"].connect(MainWindow.filter_changed)
        self.what.textChanged["QString"].connect(MainWindow.filter_changed)
        self.when_to_time.timeChanged["QTime"].connect(MainWindow.filter_changed)
        self.when_to_date.dateChanged["QDate"].connect(MainWindow.filter_changed)
        self.when_from_time.timeChanged["QTime"].connect(MainWindow.filter_changed)
        self.when_from_date.dateChanged["QDate"].connect(MainWindow.filter_changed)
        self.refreshGraph.clicked.connect(MainWindow.refresh_graph)
        self.graph_inputs_filter.stateChanged["int"].connect(
            MainWindow.graph_filter_changed
        )
        self.graph_methods_filter.stateChanged["int"].connect(
            MainWindow.graph_filter_changed
        )
        self.graph_states_filter.stateChanged["int"].connect(
            MainWindow.graph_filter_changed
        )
        self.graph_options_filter.stateChanged["int"].connect(
            MainWindow.graph_filter_changed
        )
        self.graph_outputs_filter.stateChanged["int"].connect(
            MainWindow.graph_filter_changed
        )
        self.graphTree.itemChanged["QTreeWidgetItem*", "int"].connect(
            MainWindow.state_changed
        )
        self.buildRemoteGraph.clicked.connect(MainWindow.build_remote_graph)
        self.destroyGraph.clicked.connect(MainWindow.destroy_graph)
        self.uploadGraph.clicked.connect(MainWindow.upload_graph)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.ServerControlGroup.setTitle(_translate("MainWindow", "Server Control"))
        self.startButton.setToolTip(
            _translate("MainWindow", "Instruct falcon to start processing.")
        )
        self.startButton.setStatusTip(
            _translate("MainWindow", "Instruct falcon to start processing.")
        )
        self.startButton.setText(_translate("MainWindow", "Start Processing"))
        self.stopButton.setToolTip(
            _translate("MainWindow", "Instruct falcon to stop processing.")
        )
        self.stopButton.setStatusTip(
            _translate("MainWindow", "Instruct falcon to stop processing.")
        )
        self.stopButton.setText(_translate("MainWindow", "Stop Processing"))
        self.quitButton.setToolTip(_translate("MainWindow", "Instruct falcon to quit."))
        self.quitButton.setStatusTip(
            _translate("MainWindow", "Instruct falcon to quit.")
        )
        self.quitButton.setText(_translate("MainWindow", "Quit Falcon Server"))
        self.MessagesGroup.setTitle(_translate("MainWindow", "Falcon messages"))
        self.FilterByTypeGroup.setTitle(
            _translate("MainWindow", "Filter by Message Type")
        )
        self.info_filter.setToolTip(
            _translate("MainWindow", "Display/hide info messages.")
        )
        self.info_filter.setStatusTip(
            _translate("MainWindow", "Display/hide info messages.")
        )
        self.info_filter.setText(_translate("MainWindow", "info"))
        self.error_filter.setToolTip(
            _translate("MainWindow", "Display/hide error and warning messages.")
        )
        self.error_filter.setStatusTip(
            _translate("MainWindow", "Display/hide error and warning messages.")
        )
        self.error_filter.setText(_translate("MainWindow", "warning/error"))
        self.debug_filter.setToolTip(
            _translate("MainWindow", "Display/hide debug messages.")
        )
        self.debug_filter.setStatusTip(
            _translate("MainWindow", "Display/hide debug messages.")
        )
        self.debug_filter.setText(_translate("MainWindow", "debug"))
        self.state_filter.setToolTip(
            _translate("MainWindow", "Display/hide state messages.")
        )
        self.state_filter.setStatusTip(
            _translate("MainWindow", "Display/hide state messages.")
        )
        self.state_filter.setText(_translate("MainWindow", "state"))
        self.event_filter.setToolTip(
            _translate("MainWindow", "Display/hide event messages.")
        )
        self.event_filter.setStatusTip(
            _translate("MainWindow", "Display/hide event messages.")
        )
        self.event_filter.setText(_translate("MainWindow", "event"))
        self.update_filter.setToolTip(
            _translate("MainWindow", "Display/hide update messages.")
        )
        self.update_filter.setStatusTip(
            _translate("MainWindow", "Display/hide update messages.")
        )
        self.update_filter.setText(_translate("MainWindow", "update"))
        self.FilterByPropertyGroup.setTitle(
            _translate("MainWindow", "Filter by Message Properties")
        )
        self.where_filter.setToolTip(
            _translate(
                "MainWindow",
                "Enable message filtering by where in source code message was generated.",
            )
        )
        self.where_filter.setStatusTip(
            _translate(
                "MainWindow",
                "Enable message filtering by where in source code message was generated.",
            )
        )
        self.where_filter.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;where&quot; filter is enabled, then messages are only displayed if the target text is found in the source code location where the message was generated (generally only availble for debug messages). If the &quot;regex&quot; option is enabled, then the target text is treated as a regular expression.</p></body></html>",
            )
        )
        self.where_filter.setText(_translate("MainWindow", "where"))
        self.when_from_filter.setToolTip(
            _translate("MainWindow", "Enable message filtering by message timestamp.")
        )
        self.when_from_filter.setStatusTip(
            _translate("MainWindow", "Enable message filtering by message timestamp.")
        )
        self.when_from_filter.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;from&quot; filter is enabled, then messages are only displayed if the the message timestamp is past the specified date and time.</p></body></html>",
            )
        )
        self.when_from_filter.setText(_translate("MainWindow", "from"))
        self.where.setToolTip(
            _translate("MainWindow", "Search string or regular expression.")
        )
        self.where.setStatusTip(
            _translate("MainWindow", "Search string or regular expression.")
        )
        self.where.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;where&quot; filter is enabled, then messages are only displayed if the target text is found in the source code location where the message was generated (generally only availble for debug messages). If the &quot;regex&quot; option is enabled, then the target text is treated as a regular expression.</p></body></html>",
            )
        )
        self.what.setToolTip(
            _translate("MainWindow", "Search string or regular expression.")
        )
        self.what.setStatusTip(
            _translate("MainWindow", "Search string or regular expression.")
        )
        self.what.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;what&quot; filter is enabled, then messages are only displayed if the target text is found in the message content. If the &quot;regex&quot; option is enabled, then the target text is treated as a regular expression.</p></body></html>",
            )
        )
        self.what_filter.setToolTip(
            _translate("MainWindow", "Enable message filtering by message content.")
        )
        self.what_filter.setStatusTip(
            _translate("MainWindow", "Enable message filtering by message content.")
        )
        self.what_filter.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;what&quot; filter is enabled, then messages are only displayed if the target text is found in the message content. If the &quot;regex&quot; option is enabled, then the target text is treated as a regular expression.</p></body></html>",
            )
        )
        self.what_filter.setText(_translate("MainWindow", "what"))
        self.when_to_filter.setToolTip(
            _translate("MainWindow", "Enable message filtering by message timestamp.")
        )
        self.when_to_filter.setStatusTip(
            _translate("MainWindow", "Enable message filtering by message timestamp.")
        )
        self.when_to_filter.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;to&quot; filter is enabled, then messages are only displayed if the the message timestamp is prior to the specified date and time.</p></body></html>",
            )
        )
        self.when_to_filter.setText(_translate("MainWindow", "to"))
        self.when_from_time.setToolTip(
            _translate("MainWindow", "Start time for message filtering.")
        )
        self.when_from_time.setStatusTip(
            _translate("MainWindow", "Start time for message filtering.")
        )
        self.when_from_time.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;from&quot; filter is enabled, then messages are only displayed if the the message timestamp is past the specified date and time.</p></body></html>",
            )
        )
        self.when_from_time.setDisplayFormat(_translate("MainWindow", "HH:mm:ss"))
        self.when_from_date.setToolTip(
            _translate("MainWindow", "Start date for message filtering.")
        )
        self.when_from_date.setStatusTip(
            _translate("MainWindow", "Start date for message filtering.")
        )
        self.when_from_date.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;from&quot; filter is enabled, then messages are only displayed if the the message timestamp is past the specified date and time.</p></body></html>",
            )
        )
        self.when_from_date.setDisplayFormat(_translate("MainWindow", "dd/MM/yy"))
        self.when_to_time.setToolTip(
            _translate("MainWindow", "End time for message filtering.")
        )
        self.when_to_time.setStatusTip(
            _translate("MainWindow", "End time for message filtering.")
        )
        self.when_to_time.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;to&quot; filter is enabled, then messages are only displayed if the the message timestamp is prior to the specified date and time.</p></body></html>",
            )
        )
        self.when_to_time.setDisplayFormat(_translate("MainWindow", "HH:mm:ss"))
        self.when_to_date.setToolTip(
            _translate("MainWindow", "End date for message filtering.")
        )
        self.when_to_date.setStatusTip(
            _translate("MainWindow", "End date for message filtering.")
        )
        self.when_to_date.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;to&quot; filter is enabled, then messages are only displayed if the the message timestamp is prior to the specified date and time.</p></body></html>",
            )
        )
        self.when_to_date.setDisplayFormat(_translate("MainWindow", "dd/MM/yy"))
        self.what_regex.setToolTip(
            _translate("MainWindow", "Select regular expression mode.")
        )
        self.what_regex.setStatusTip(
            _translate("MainWindow", "Select regular expression mode.")
        )
        self.what_regex.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;what&quot; filter is enabled, then messages are only displayed if the target text is found in the message content. If the &quot;regex&quot; option is enabled, then the target text is treated as a regular expression.</p></body></html>",
            )
        )
        self.what_regex.setText(_translate("MainWindow", "regex"))
        self.where_regex.setToolTip(
            _translate("MainWindow", "Select regular expression mode.")
        )
        self.where_regex.setStatusTip(
            _translate("MainWindow", "Select regular expression mode.")
        )
        self.where_regex.setWhatsThis(
            _translate(
                "MainWindow",
                "<html><head/><body><p>If the &quot;where&quot; filter is enabled, then messages are only displayed if the target text is found in the source code location where the message was generated (generally only availble for debug messages). If the &quot;regex&quot; option is enabled, then the target text is treated as a regular expression.</p></body></html>",
            )
        )
        self.where_regex.setText(_translate("MainWindow", "regex"))
        self.applyFilters.setToolTip(
            _translate("MainWindow", "Apply filters to all received messages.")
        )
        self.applyFilters.setStatusTip(
            _translate("MainWindow", "Apply filters to all received messages.")
        )
        self.applyFilters.setText(_translate("MainWindow", "Apply Filters"))
        self.revertFilters.setToolTip(
            _translate("MainWindow", "Undo changes to filter settings.")
        )
        self.revertFilters.setStatusTip(
            _translate("MainWindow", "Undo changes to filter settings.")
        )
        self.revertFilters.setText(_translate("MainWindow", "Revert Filters"))
        self.clearMessages.setToolTip(
            _translate("MainWindow", "Clear history of all received messages.")
        )
        self.clearMessages.setStatusTip(
            _translate("MainWindow", "Clear history of all received messages.")
        )
        self.clearMessages.setText(_translate("MainWindow", "Clear Messages"))
        self.GraphGroup.setTitle(_translate("MainWindow", "Processing Graph"))
        self.buildRemoteGraph.setToolTip(
            _translate("MainWindow", "Build an existing server-side graph")
        )
        self.buildRemoteGraph.setStatusTip(
            _translate("MainWindow", "Build an existing server-side graph")
        )
        self.buildRemoteGraph.setText(_translate("MainWindow", "Build Remote Graph"))
        self.uploadGraph.setToolTip(
            _translate("MainWindow", "Upload a locally defined graph to server")
        )
        self.uploadGraph.setStatusTip(
            _translate("MainWindow", "Upload a locally defined graph to server")
        )
        self.uploadGraph.setText(_translate("MainWindow", "Upload Graph"))
        self.destroyGraph.setToolTip(
            _translate("MainWindow", "Destroy server-side graph")
        )
        self.destroyGraph.setStatusTip(
            _translate("MainWindow", "Destroy server-side graph")
        )
        self.destroyGraph.setText(_translate("MainWindow", "Destroy Graph"))
        self.refreshGraph.setToolTip(
            _translate("MainWindow", "Download graph definition from server")
        )
        self.refreshGraph.setStatusTip(
            _translate("MainWindow", "Download graph definition from server")
        )
        self.refreshGraph.setText(_translate("MainWindow", "Refresh Graph"))
        self.groupBox.setTitle(_translate("MainWindow", "Filter Graph Elements"))
        self.graph_options_filter.setToolTip(
            _translate("MainWindow", "Display/hide processor options.")
        )
        self.graph_options_filter.setStatusTip(
            _translate("MainWindow", "Display/hide processor options.")
        )
        self.graph_options_filter.setText(_translate("MainWindow", "options"))
        self.graph_outputs_filter.setToolTip(
            _translate("MainWindow", "Display/hide output port descriptions.")
        )
        self.graph_outputs_filter.setStatusTip(
            _translate("MainWindow", "Display/hide output port descriptions.")
        )
        self.graph_outputs_filter.setText(_translate("MainWindow", "outputs"))
        self.graph_inputs_filter.setToolTip(
            _translate("MainWindow", "Display/hide input port descriptions.")
        )
        self.graph_inputs_filter.setStatusTip(
            _translate("MainWindow", "Display/hide input port descriptions.")
        )
        self.graph_inputs_filter.setText(_translate("MainWindow", "inputs"))
        self.graph_states_filter.setToolTip(
            _translate("MainWindow", "Display/hide processor states.")
        )
        self.graph_states_filter.setStatusTip(
            _translate("MainWindow", "Display/hide processor states.")
        )
        self.graph_states_filter.setText(_translate("MainWindow", "states"))
        self.graph_methods_filter.setToolTip(
            _translate("MainWindow", "Display/hide processor methods.")
        )
        self.graph_methods_filter.setStatusTip(
            _translate("MainWindow", "Display/hide processor methods.")
        )
        self.graph_methods_filter.setText(_translate("MainWindow", "methods"))
        self.graphTree.setSortingEnabled(True)
        self.graphTree.headerItem().setText(0, _translate("MainWindow", "Key"))
        self.graphTree.headerItem().setText(1, _translate("MainWindow", "Value"))
