from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt


class YamlEditor(QtWidgets.QPlainTextEdit):
    def __init__(self, parent=None):

        QtWidgets.QPlainTextEdit.__init__(self, parent)

        # option = self.document().defaultTextOption()
        # option.setFlags( option.flags() | QtGui.QTextOption.ShowTabsAndSpaces )
        # self.document().setDefaultTextOption(option)

    def keyPressEvent(self, event):
        txt = event.text()

        if event.key() == Qt.Key_Tab:
            txt = "    "

        fake_event = QtGui.QKeyEvent(
            event.type(),
            event.key(),
            event.modifiers(),
            txt,
            event.isAutoRepeat(),
            event.count(),
        )

        QtWidgets.QPlainTextEdit.keyPressEvent(self, fake_event)
