import datetime
import inspect
from weakref import WeakKeyDictionary
from weakref import WeakSet

import yaml
import zmq


class FalconError(Exception):
    pass


class FalconConnectionError(FalconError):
    pass


class FalconRequestError(FalconError):
    pass


class Falcon:
    def __init__(self, address="localhost", port=5555, context=None):
        self._ip_address = str(address)
        self._port = int(port)

        self._context = context or zmq.Context.instance()
        self._poller = zmq.Poller()
        self._request_socket = None

        self._connected = False
        self._connect()

    def _connect(self):
        if self.connected:
            return

        self._request_socket = self._context.socket(zmq.REQ)
        self._request_socket.connect(self.server_address)
        self._poller.register(self._request_socket, zmq.POLLIN)
        self._connected = True

    def _disconnect(self):
        if not self.connected:
            return

        self._request_socket.setsockopt(zmq.LINGER, 0)
        self._request_socket.close()
        self._poller.unregister(self._request_socket)
        self._request_socket = None
        self._connected = False

    def _reconnect(self):
        self._disconnect()
        self._connect()

    def request(self, *commands, **kwargs):
        if not self._connected:
            self._connect()
        commands = [c if isinstance(c, bytes) else c.encode("utf-8") for c in commands]
        self._request_socket.send_multipart(commands)

        socks = dict(self._poller.poll(kwargs.get("timeout", 2000)))
        if socks.get(self._request_socket) == zmq.POLLIN:
            reply = self._request_socket.recv_multipart()
        else:
            self._disconnect()
            raise FalconConnectionError("Falcon is not online.")

        return reply

    @property
    def connected(self):
        return self._connected

    @property
    def ip_address(self):
        return self._ip_address

    @ip_address.setter
    def ip_address(self, value):
        self._ip_adress = str(value)
        self._reconnect()

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = int(value)
        self._reconnect()

    @property
    def alive(self):
        try:
            self.request("heart", timeout=500)
        except:
            return False

        return True

    @property
    def server_address(self):
        return "tcp://{ip}:{port}".format(ip=self._ip_address, port=self._port)

    def server_info(self):
        reply = self.request("info")
        return yaml.load(reply[0], Loader=yaml.FullLoader)

    def server_kill(self):
        return self.request("kill")

    def server_quit(self):
        reply = self.request("quit")
        if reply[0] != b"OK":
            raise FalconRequestError(reply[1])

    def server_test(self, enable=None):
        if enable is None:
            reply = self.request("test")  # toggle test flag
        else:
            reply = self.request("test", str(enable).lower())

        if reply[0] != b"OK":
            raise FalconRequestError(reply[1])

        reply = self.server_info()
        return reply["default_test_flag"]

    def graph_build(self, graph, replace=False):

        if replace:
            # make sure to destroy any existing graph
            # this will still raise an exception if a graph is running
            self.graph_destroy()

        reply = self.request("graph", "build", yaml.dump(graph))
        if reply[0] != b"OK":
            raise FalconRequestError(reply[1])

    def graph_buildfile(self, filename, replace=False):

        if replace:
            # make sure to destroy any existing graph
            # this will still raise an exception if a graph is running
            self.graph_destroy()

        reply = self.request("graph", "build", str(filename))
        if reply[0] != b"OK":
            raise FalconRequestError(reply[1])

    def graph_state(self):
        return self.request("graph", "state")[0]

    def graph_destroy(self):
        reply = self.request("graph", "destroy")
        if reply[0] != b"OK":
            raise FalconRequestError(reply[1])

    def graph_start(self, run_env="", destination="", source="", test=None):
        command = "start"
        if test:
            command = "test"
        reply = self.request(
            "graph", command, str(run_env), str(destination), str(source)
        )
        print("try to start")
        if reply[0] != b"OK":
            raise FalconRequestError(reply[1])

    def graph_stop(self):
        reply = self.request("graph", "stop")
        if reply[0] != b"OK":
            raise FalconRequestError(reply[1])

    def graph_definition(self):
        reply = self.request("graph", "yaml")
        return yaml.load(reply[0], Loader=yaml.FullLoader)

    def _graph_yaml_request(self, command, request):
        # request is str or dict
        if not isinstance(request, str):
            request = yaml.dump(request)

        reply = self.request("graph", command, request)
        return yaml.load(reply[0], Loader=yaml.FullLoader)

    def graph_retrieve(self, request):
        return self._graph_yaml_request("retrieve", request)

    def graph_update(self, request):
        return self._graph_yaml_request("update", request)

    def graph_apply(self, request):
        return self._graph_yaml_request("apply", request)

    def graph_processors(self):
        p = []
        g = self.graph_definition()
        if g is not None:
            p = g.get("processors", dict()).keys()
        return p

    def retrieve_state(self, processor, state):
        request = "{{{processor}: {{{state}: 0}}}}".format(
            processor=str(processor), state=str(state)
        )
        reply = self.graph_retrieve(request)
        return self._transform_value_str(reply[str(processor)][str(state)])

    def update_state(self, processor, state, value):
        value=self._transform_value_str(value)
        request = "{{{processor}: {{{state}: {value}}}}}".format(
            processor=str(processor), state=str(state), value=value
        )

        reply = self.graph_update(request)
        return self._transform_value_str(reply[str(processor)][str(state)])

    def _transform_value_str(self, value):
        if value in ["False", "0"]:
            return "false"
        elif value in ["True", "1"]:
            return "true"
        return str(value)

    def apply_method(self, processor, method, **kwargs):
        request = "{{{processor}: {{{method}: {value}}}}}".format(
            processor=str(processor), method=str(method), value=yaml.dump(kwargs)
        )
        reply = self.graph_apply(request)
        return reply


class FalconEvent:
    def __init__(self):
        self._functions = WeakSet()
        self._methods = WeakKeyDictionary()

    def __call__(self, *args, **kargs):
        # Call handler functions
        for func in self._functions:
            func(*args, **kargs)

        # Call handler methods
        for obj, funcs in self._methods.items():
            for func in funcs:
                func(obj, *args, **kargs)

    def connect(self, slot):
        if inspect.ismethod(slot):
            if slot.__self__ not in self._methods:
                self._methods[slot.__self__] = set()

            self._methods[slot.__self__].add(slot.__func__)

        else:
            self._functions.add(slot)

    def disconnect(self, slot):
        if inspect.ismethod(slot):
            if slot.__self__ in self._methods:
                self._methods[slot.__self__].remove(slot.__func__)
        else:
            if slot in self._functions:
                self._functions.remove(slot)

    def clear(self):
        self._functions.clear()
        self._methods.clear()


class FalconEvents:
    def __init__(self, address="localhost", port=5556, context=None):
        self._ip_address = str(address)
        self._port = int(port)

        self._context = context or zmq.Context.instance()
        self._poller = zmq.Poller()
        self._socket = None

        self._events = dict(
            debug=FalconEvent(),
            info=FalconEvent(),
            update=FalconEvent(),
            state=FalconEvent(),
        )

        self._connect()

    @property
    def debug(self):
        return self._events["debug"]

    @property
    def info(self):
        return self._events["info"]

    @property
    def update(self):
        return self._events["update"]

    @property
    def state(self):
        return self._events["state"]

    def _connect(self):
        self._socket = self._context.socket(zmq.SUB)
        # self._socket.RCVTIMEO = 1000
        self._socket.connect(self.server_address)
        self._socket.setsockopt(zmq.SUBSCRIBE, b"")
        self._poller.register(self._socket, zmq.POLLIN)

    def _disconnect(self):
        self._socket.setsockopt(zmq.LINGER, 0)
        self._socket.close()
        self._poller.unregister(self._socket)
        self._socket = None

    def _reconnect(self):
        self._disconnect()
        self._connect()

    @property
    def connected(self):
        return self._socket is not None

    @property
    def server_address(self):
        return "tcp://{ip}:{port}".format(ip=self._ip_address, port=self._port)

    def poll(self):
        s = dict(self._poller.poll())

        if self._socket in s and s[self._socket] == zmq.POLLIN:
            return True

        return False

    def get_next_event(self):
        message = self._socket.recv_multipart()
        message = [c if isinstance(c, str) else c.decode("utf-8") for c in message]
        return message

    def parse_event(self, message):
        try:
            d = dict(
                kind=message[0].lower(),
                when=datetime.datetime.strptime(message[1], "%Y/%m/%d %H:%M:%S %f"),
                what=message[2],
            )
        except:
            d = dict(
                kind=message[0].lower(),
                when=datetime.datetime.strptime(message[1], "%Y/%m/%d %H:%M:%S"),
                what=message[2],
            )
        if len(message) > 3:
            d["where"] = message[3]

        return d

    def process_next_event(self):
        event = self.parse_event(self.get_next_event())

        if event["kind"] in self._events:
            self._events[event["kind"]](event)
