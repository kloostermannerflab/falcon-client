# -*- coding: utf-8 -*-
"""
Created on Fri Mar 14 16:01:18 2014

@author: davide
"""

import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import iirfilter, zpk2sos
from fklab.signals import filter


#parameters of the filter
sample_rate = 30000

N = 8 # desired final order
max_ripple_pass = 1 #for Chebyshev and elliptic filters, provides the maximum ripple in the passband. (dB)
max_ripple_stop = 40 #for Chebyshev and elliptic filters, provides the minimum attenuation in the stop band. (dB)
band_type = 'bandpass'
#filter_type = 'butter'
#filter_type = 'cheby1'
#filter_type = 'cheby2'
filter_type = 'cheby2'
#filter_type = 'bessel'
low_cut_hz = 115
high_cut_hz = 275


# where the sos coefficients that will be read by Falcon should go
save_filter = True
name = filter_type + '_ripple_' + str(int(sample_rate*1e-3)) + "kHz"
filepath_sos = "/home/chaputmarine/Documents/filters/iir_ripple/" + name + ".filter"
filepath_ba = "/home/chaputmarine/Documents/filters/iir_ripple/" + name + "_ba.npy"

# compute Nyquist frequency
nyq_hz = sample_rate/2

#create inputs for filter design
if band_type == 'bandpass':
    Wn = [low_cut_hz/nyq_hz, high_cut_hz/nyq_hz]
elif band_type == 'lowpass':
    Wn = high_cut_hz/nyq_hz
elif band_type == 'highpass':
    Wn = low_cut_hz/nyq_hz
    
# compute number of 2nd-order stages    
n_stages = int(N/2 )   
    
# design filter in different outputs
if band_type == 'bandpass':
    N = int(N / 2)

b, a = iirfilter(N, Wn, btype=band_type, ftype=filter_type,rp=max_ripple_pass, rs=max_ripple_stop, output='ba')
z, p, gain = iirfilter(N, Wn, btype=band_type, ftype=filter_type, rp=max_ripple_pass, rs=max_ripple_stop, output='zpk')

# compute SOS coefficients with unitary gain
sos = zpk2sos(z, p, gain)
sos[0, :3] /= sos[0][0]

# compute order and check stability
order = max([len(a) - 1, len(b) - 1])
print("filter order is: " + str(order))
if (np.all(np.abs(np.roots(a)) < 1)):
    print("the designed IIR filter is stable")
else:
    print("the designed IIR filter is NOT stable")

# plot frequency responses
filter.inspect_filter(b, a, fs=sample_rate, detail=(0, nyq_hz/2), grid=True)

# save filter coefficients in formats that Falcon and Python understands easily
if save_filter:
    with open(filepath_sos, "w") as f:
        # add header
        f.write("##")    
        f.write("\n# format = text")
        f.write("\n# type = biquad")
        f.write("\n# description = ")
        f.write(str(order) + "th order " + filter_type + " " + band_type +\
            " filter with cutoffs at " + str(low_cut_hz) + " - " + \
            str(high_cut_hz) + " Hz (sampling frequency = " + \
            str(sample_rate) + " Hz)")
        f.write("\n#\n# max_ripple_pass = " + str(max_ripple_pass) )
        f.write("\n# max_ripple_stop = " + str(max_ripple_stop) )
        f.write("\n#\n# Header automatically added from custom-made Python script iir_design.py")
        f.write("\n##\n")
        f.write("\n".join(" ".join(map(str, x)) for x in tuple( [[gain]] + sos.tolist()) ) )
        f.close()
print("Filter SOS coefficients saved in " + filepath_sos)
np.save(filepath_ba, (b, a))
print("Filter ba coefficients saved in " + filepath_ba)

plt.show()
