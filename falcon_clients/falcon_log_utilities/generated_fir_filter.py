import scipy as sp
import scipy.signal
import numpy as np
numtaps = 101
cutoff = [115, 275]
pass_zero = "bandpass"
fs= 30000

f = sp.signal.firwin(numtaps=numtaps, cutoff=cutoff, pass_zero=pass_zero, fs=fs)
f.tofile( "/home/chaputmarine/Documents/filters/fir_lfp_0.1fs.filter", sep="\n" )
np.save("/home/chaputmarine/Documents/filters/fir_lfp_0.1fs.npy", f)
