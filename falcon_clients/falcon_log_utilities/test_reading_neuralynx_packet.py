import argparse
import socket
import sys


def main():
    parser = argparse.ArgumentParser(
        description="Simple Falcon client"
    )
    parser.add_argument("--ip", default="127.0.0.1", help="Neuralynx ip-address")
    parser.add_argument("--port", default=5000, help="Neuralynx port")

    args = parser.parse_args()

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
#    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, None, sys.getsizeof(int))

    s.bind((args.ip, args.port))

    while True:
        print("####### Server is listening #######")
        data, address = s.recvfrom(584, 0)
        print(data.decode("utf-16-le"))





if __name__ == "__main__":
    main()