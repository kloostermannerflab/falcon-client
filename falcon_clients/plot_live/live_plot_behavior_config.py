# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 16:17:22 2016

@author: davide
"""
import numpy as np

primary_address = "tcp://localhost:7777"

time_history = 30  # [s]
sampling_period = 0.04  # [s]
speed_ticks = np.linspace(0, 30, 5)
position_ticks = np.linspace(0, 200, 10, dtype="int64")
