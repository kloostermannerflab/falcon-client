import _thread
import time

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import zmq

import falcon_clients.plot_live.live_plot_behavior_config as config

dt = np.dtype(
    [
        ("stream", (np.uint16, 1)),
        ("packet", np.uint64(1)),
        ("source_ts", np.uint64(1)),
        ("hardware_ts", np.uint64(1)),
        ("serial_number", np.uint64(1)),
        ("linear_position", (np.float64, 1)),
        ("speed", (np.float64, 1)),
        ("unit", (np.str_, 5)),
        ("speed_sign", (np.bool, 1)),
    ]
)

finished = False

mpl.rcParams["xtick.labelsize"] = 20


def plot_behavior_live():
    global finished

    context = zmq.Context()
    socket1 = context.socket(zmq.SUB)
    socket1.setsockopt(zmq.SUBSCRIBE, b"")
    socket1.connect(config.primary_address)

    clip_value_speed = config.speed_ticks[-1]
    clip_value_position = config.position_ticks[-1]
    time_plot = np.arange(0, config.time_history, config.sampling_period)
    buffer_size = len(time_plot)

    speed = np.zeros(buffer_size)
    position = np.zeros(buffer_size)

    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212, sharex=ax1)

    ax1.set_xlabel(" Time [s] ", fontsize=15, weight="bold")
    ax1.set_ylabel(" Speed [cm/s] ", fontsize=25, weight="bold", color="green")
    ax1.set_ylim([0, config.speed_ticks[-1]])
    ax1.set_xlim(config.time_history)
    ax1.set_yticks(config.speed_ticks)
    ax1.set_yticklabels(config.speed_ticks, weight="bold", fontsize=15)
    ax1.grid(True, linestyle="-", color="0.75")
    hSpeed_line = mpl.lines.Line2D(time_plot, speed, color="green", linewidth=1.5)
    ax1.add_line(hSpeed_line)

    ax2.set_xlabel(" Time [s] ", fontsize=15, weight="bold")
    ax2.set_ylabel(
        " Linearized position [cm] ", fontsize=25, weight="bold", color="blue"
    )
    ax2.set_ylim([0, config.position_ticks[-1]])
    ax2.set_xlim(config.time_history)
    ax2.set_yticks(config.position_ticks)
    ax2.set_yticklabels(config.position_ticks, weight="bold", fontsize=15)
    ax2.grid(True, linestyle="-", color="0.75")
    hPosition_line = mpl.lines.Line2D(time_plot, position, color="blue", linewidth=1.5)
    ax2.add_line(hPosition_line)

    plt.ion()
    plt.show()

    while not finished:

        # get data
        data = socket1.recv()
        data = np.frombuffer(data, dtype=dt)

        speed[1:] = speed[:-1]
        position[1:] = position[:-1]

        # fix data
        if data["speed"][0] < clip_value_speed:
            speed[0] = data["speed"][0]
        else:
            speed[0] = clip_value_speed

        if data["linear_position"][0] < clip_value_position:
            position[0] = data["linear_position"][0]
        else:
            position[0] = clip_value_position

        # update plot and last timestamp
        hSpeed_line.set_ydata(speed)
        hPosition_line.set_ydata(position)
        ax1.set_title(
            str(data["hardware_ts"][0] / 1e6) + " s", fontsize=30, weight="bold"
        )

        plt.draw()
        plt.pause(0.001)

    plt.close()


def main():
    global finished

    _thread.start_new_thread(plot_behavior_live, ())
    time.sleep(0.5)

    while not finished:
        input("\nPress any key to quit the live plotting: \n\n")
        finished = True


if __name__ == "__main__":
    main()
