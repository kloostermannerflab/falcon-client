# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 18:13:25 2016

@author: davide
"""
import numpy as np

time_history = 80  # [s]
sampling_period = 0.25  # [s]
ticks = np.array([0, 2.5, 5, 7.5, 10, 12.5, 15, 20, 25, 30, 31])
