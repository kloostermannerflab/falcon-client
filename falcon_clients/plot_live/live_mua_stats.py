import _thread
import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import zmq

import falcon_clients.plot_live.live_mua_stats_config as config

dt = np.dtype([("timestamp", (np.uint64, 1)), ("signal", (np.double, 2))])

finished = False


def plot_ripple_stats_live():

    global finished

    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.setsockopt(zmq.SUBSCRIBE, b"")
    socket.connect("tcp://localhost:7777")

    # create axes for plotting
    hAx = plt.axes()

    # create lists of line (one for each buffer)
    hThreshold = []
    hSignal = []

    for b in range(config.nbuffers):

        line = matplotlib.lines.Line2D([0], [0], color="green")
        hSignal.append(line)
        hAx.add_line(line)

        line = matplotlib.lines.Line2D([0], [0], color="r")
        hThreshold.append(line)
        hAx.add_line(line)

    # set fixed y-axis limits
    plt.ylim([0, config.ylim])

    plt.ion()
    plt.show()

    buffer_index = 0

    start_time = 0

    while not finished:

        # get data
        data = socket.recv()
        data = np.frombuffer(data, dtype=dt)

        # extract time
        time = data["timestamp"] / 1e6 - start_time

        if start_time == 0:
            start_time = time[0]

        # update plot
        hSignal[buffer_index].set_xdata(time)
        hSignal[buffer_index].set_ydata(data["signal"][:, 0])

        hThreshold[buffer_index].set_xdata(time)
        hThreshold[buffer_index].set_ydata(data["signal"][:, 1])

        plt.xlim(time[-1] - config.nbuffers * (time[-1] - time[0]), time[-1])

        plt.title("last timestamp = " + str(time[-1] + start_time) + " s")

        plt.ylabel("MUA [spikes/s]")
        plt.xlabel("time from start [s]")

        plt.draw()
        plt.pause(0.001)

        buffer_index = buffer_index + 1
        if buffer_index == config.nbuffers:
            buffer_index = 0


def main():
    global finished

    _thread.start_new_thread(plot_ripple_stats_live, ())
    time.sleep(2)

    while not finished:
        input("\nPress any key to quit the live plotting: \n\n")
        finished = True


# call main
if __name__ == "__main__":
    main()
