import _thread
import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import zmq

import falcon_clients.plot_live.live_plot_decoding_error_config as config

dt = np.dtype(
    [
        ("stream", (np.uint16, 1)),
        ("packet", (np.uint64, 1)),
        ("source_ts", (np.uint64, 1)),
        ("hardware_ts", (np.uint64, 1)),
        ("serial_number", (np.uint64, 1)),
        ("scalar_data", (np.float64, 1)),
    ]
)

finished = False


def plot_error_live(second_socket=True, second_address="tcp://localhost:7778"):
    global finished

    context = zmq.Context()
    socket1 = context.socket(zmq.SUB)
    socket1.setsockopt(zmq.SUBSCRIBE, b"")
    socket1.connect("tcp://localhost:7777")

    if second_socket:
        socket2 = context.socket(zmq.SUB)
        socket2.setsockopt(zmq.SUBSCRIBE, b"")
        socket2.connect(second_address)

    clip_value = config.ticks[-2]
    time_plot = np.arange(0, config.time_history, config.sampling_period)
    buffer_size = len(time_plot)

    # set fixed y-axis limits
    plt.ylim([0, config.ticks[-1]])
    plt.xlim(config.time_history)
    plt.yticks(config.ticks, weight="bold", fontsize=15)
    plt.xticks(weight="bold", fontsize=15)
    plt.ylabel(" Decoding error [cm] ", fontsize=25, weight="bold")
    plt.xlabel(" Time [s] ", fontsize=15, weight="bold")
    plt.figtext(
        0.25,
        0.96,
        "decoding_error",
        fontsize="xx-large",
        color="blue",
        ha="right",
        weight="bold",
    )
    plt.figtext(
        0.7,
        0.96,
        "median decoding error Falcon",
        fontsize="xx-large",
        color="red",
        ha="right",
        weight="bold",
    )
    plt.figtext(
        1,
        0.96,
        "median decoding error display",
        fontsize="xx-large",
        color="black",
        ha="right",
        weight="bold",
    )

    plt.ion()
    plt.show()

    current_error = np.zeros(buffer_size)
    median_error_from_network = np.zeros(buffer_size)

    # create axes and line for plotting
    hAx = plt.axes()
    hDecodingError_line = matplotlib.lines.Line2D(
        time_plot, current_error, color="b", linewidth=1.5
    )
    hMedianDecodingError_line = matplotlib.lines.Line2D(
        time_plot, current_error, color="k", linewidth=1.5
    )  # horizontal line
    hMedianDecodingErrorFromNetwork_line = matplotlib.lines.Line2D(
        time_plot, current_error, color="r", linewidth=1.5
    )
    hAx.add_line(hDecodingError_line)
    hAx.add_line(hMedianDecodingError_line)
    if second_socket:
        hAx.add_line(hMedianDecodingErrorFromNetwork_line)
    hAx.grid()

    while not finished:
        # get data
        data1 = socket1.recv()
        data1 = np.frombuffer(data1, dtype=dt)
        if second_socket:
            data2 = socket2.recv()
            data2 = np.frombuffer(data2, dtype=dt)

        current_error[1:] = current_error[:-1]
        median_error_from_network[1:] = median_error_from_network[:-1]
        if data1["scalar_data"][0] < clip_value:
            current_error[0] = data1["scalar_data"][0]
        else:
            current_error[0] = clip_value
        if second_socket:
            if data2["scalar_data"][0] < clip_value:
                median_error_from_network[0] = data2["scalar_data"][0]
            else:
                median_error_from_network[0] = clip_value

        # update plot
        hDecodingError_line.set_ydata(current_error)
        hMedianDecodingErrorFromNetwork_line.set_ydata(median_error_from_network)
        hMedianDecodingError_line.set_ydata(np.median(current_error))

        plt.draw()
        plt.pause(0.001)

    plt.close()


def main():
    global finished

    _thread.start_new_thread(plot_error_live, ())
    time.sleep(2)

    while not finished:
        k = input("\nPress 'q' or 'Q' to quit the live plotting: ")

        if k == "q" or k == "Q":
            finished = True


# call main
if __name__ == "__main__":
    main()
