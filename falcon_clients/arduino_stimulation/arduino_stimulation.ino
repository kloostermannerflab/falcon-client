/*
 Send TTL pulses when data is received.
TTL1: detection
TTL2: stimulation, pulse 1
TTL3: stimulation, pulse 2
two TTL stimuli are generated to trigger a biphasic stimulation. This is necessary when you cannot program the stimulatior directly. If you can programm the stimulator to send a biphasic current upon 1 trigger (1 TTL pusle) use the script send_det&stim_TTL.ino.
 */


int TTLDetectionPin = 2;     
int TTLStimulationPin = 7;   
int BAUDRATE = 9600;  // bit per second
int ttlDur = 100;    // pulse duration for detection TTL in microsec.
int pulseDurationDoublePulse = 100; // pulse duration of one of the two pulses in the biphasic pulse in microsec.
char buffer = 'n';

void setup() {
  pinMode(TTLDetectionPin, OUTPUT);    // declare the TTL pin as output
  pinMode(TTLStimulationPin, OUTPUT);
  Serial.begin(BAUDRATE);     // connect to the serial port at BAUDRATE bits per second
  Serial.setTimeout(1); // 1 ms time-out for serial readout
}

void loop () {
  Serial.readBytes(&buffer, 1);
    if(buffer=='o'){ // detection + stimulation
      digitalWrite(TTLDetectionPin, HIGH);
      delayMicroseconds(ttlDur);
      digitalWrite(TTLDetectionPin, LOW);
      double_pulse_stimulation(TTLStimulationPin,pulseDurationDoublePulse);
    }else if(buffer=='d'){ // stimulation
      double_pulse_stimulation(TTLStimulationPin,pulseDurationDoublePulse);
    }else if ( buffer=='r') { // detection stim
      digitalWrite(TTLDetectionPin, HIGH);
      delayMicroseconds(ttlDur);
      digitalWrite(TTLDetectionPin, LOW);    
    }
  buffer = 'n';
}



void double_pulse_stimulation (int pin, int pd) {
  generate_TTL(pin,pd);
  delayMicroseconds(55); 
  generate_TTL(pin,pd);
}
void generate_TTL(int pin, int duration)
{
  digitalWrite(pin, HIGH);
  delayMicroseconds(duration);
  digitalWrite(pin,LOW);
}
