
[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)
![Falcon version](https://img.shields.io/badge/Falcon-v1.0.0-blue)
    [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Falcon client is a collection of graphique interface used with Falcon.

# Install

## mode user
```bash
   conda install -c KloostermanLab falcon_clients
```

## mode developper

```bash
   git clone https://bitbucket.org/kloostermannerflab/falcon-client.git
   cd falcon-client
   python setup.py build_ext --inplace
   pip install -e .
```

## Troobleshoot

This version have been updated to work with python 3 and pyqt5. If you are unhappy with this modification, add an issue describing your issue and in waiting come back to the previous running version (python 2.x and pyqt4).

```bash
   pip install git+https://bitbucket.org/kloostermannerflab/falcon-client/src/1.0/
```

# Simple client

**Bash command** :
```bash
   simple_client
```

This ui is used to control easily the workflow around Falcon. Once a running falcon instance is existing, the ui will be able to send and receive informations to it (in local or in remote if ip adress and port are carefully chosen).
A graph can be started, stopped, modified and loaded. The information sent back by falcon can be selected by using filters of times and types.


# Vizualization ui

There are, for the moment, 4 kinds of vizualization ui which can be launch with their own command:

**Bash command** :
```bash
   live_plot_mua_stats
   live_plot_behavior
   live_plot_decoding_error
   live_plot_ripple_stats
```

Display the output of the graph running in Falcon.

# Contributing

# How to contribute

## Open an issue :

1. Specify in the title the client concerned
2. An use-case allowing to easily reproduce a bug is always appreciate

Issue are always welcome, don't hesitate to share your thought about this module.

## Add a new visualization client :

Don't hesitate to fork the repository and open a pull request.
Interesting new features could be a new visualization ui for a different type of falcon output.
