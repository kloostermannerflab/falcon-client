alabaster==0.7.12
appdirs==1.4.3
Babel @ file:///tmp/build/80754af9/babel_1607110387436/work
bottle @ file:///home/conda/feedstock_root/build_artifacts/bottle_1591809651423/work
breathe @ file:///home/conda/feedstock_root/build_artifacts/breathe_1606919631860/work
brotlipy==0.7.0
certifi==2020.12.5
cffi @ file:///tmp/build/80754af9/cffi_1598370813909/work
chardet==3.0.4
colorama==0.4.3
conan @ file:///home/conda/feedstock_root/build_artifacts/conan_1599049536682/work
cryptography @ file:///tmp/build/80754af9/cryptography_1598892037289/work
cycler==0.10.0
deprecation==2.0.6
distro==1.1.0
docutils==0.16
-e git+git@bitbucket.org:kloostermannerflab/falcon-client.git@a9bd8c695c8b742e116c498a157d716e24713839#egg=falcon_clients
fasteners==0.15
fklab-cmake-gui @ file:///opt/conda/conda-bld/fklab-cmake-gui_1594310779531/work
future==0.18.2
-e git+git@bitbucket.org:kloostermannerflab/fklab-controller-lab.git@ec33e05f222ebb82d2337b21316864ac6fdc90ca#egg=hive
idna @ file:///tmp/build/80754af9/idna_1593446292537/work
imagesize==1.2.0
Jinja2==2.11.2
kiwisolver==1.2.0
livereload==2.6.3
MarkupSafe @ file:///tmp/build/80754af9/markupsafe_1594371495811/work
matplotlib @ file:///home/conda/feedstock_root/build_artifacts/matplotlib-base_1600159101071/work
mkl-fft==1.1.0
mkl-random==1.1.1
mkl-service==2.3.0
monotonic==1.5
node-semver==0.6.1
numpy @ file:///tmp/build/80754af9/numpy_and_numpy_base_1596233707986/work
olefile==0.46
packaging==20.4
patch-ng==1.17.4
Pillow @ file:///tmp/build/80754af9/pillow_1594307325547/work
pkgconfig==1.4.0
pluginbase==0.7
pycparser @ file:///tmp/build/80754af9/pycparser_1594388511720/work
Pygments==2.6.1
PyJWT==1.7.1
pyOpenSSL @ file:///tmp/build/80754af9/pyopenssl_1594392929924/work
pyparsing==2.4.7
PySocks @ file:///tmp/build/80754af9/pysocks_1594394576006/work
python-dateutil==2.8.1
pytz @ file:///tmp/build/80754af9/pytz_1606604771399/work
PyYAML==5.3.1
pyzmq==19.0.1
requests @ file:///tmp/build/80754af9/requests_1592841827918/work
rstcheck==3.3.1
six==1.14.0
snowballstemmer==2.0.0
Sphinx @ file:///home/conda/feedstock_root/build_artifacts/sphinx_1605196202148/work
sphinx-autobuild==2020.9.1
sphinx-bootstrap-theme==0.8.0
sphinxcontrib-applehelp==1.0.2
sphinxcontrib-devhelp==1.0.2
sphinxcontrib-htmlhelp==1.0.3
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.3
sphinxcontrib-serializinghtml==1.1.4
tornado==6.0.4
tqdm @ file:///tmp/build/80754af9/tqdm_1596810128862/work
urllib3 @ file:///tmp/build/80754af9/urllib3_1597086586889/work
