from setuptools import setup
from distutils.core import Extension

import re
from os import path

from setuptools import setup


def read_file(file_path):
    here = path.abspath(path.dirname(__file__))
    with open(path.join(here, file_path), encoding="utf-8") as f:
        return f.read()


def get_version():
    VERSIONFILE = "falcon_clients/_version.py"
    verstrline = open(VERSIONFILE, "rt").read()
    VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
    mo = re.search(VSRE, verstrline, re.M)
    if mo:
        return mo.group(1)
    raise RuntimeError("Unable to find version string in %s." % (VERSIONFILE,))


packages = [
    "falcon_clients",
    "falcon_clients.server_control.ui.simple_client",
    "falcon_clients.server_control.ui.pyqt",
    "falcon_clients.plot_live",
]

setup(
    name="falcon_clients",
    version=get_version(),
    packages=packages,
    entry_points={
        "console_scripts": [
            "simple_client=falcon_clients.server_control.ui.simple_client.simple_client:main",
            "live_plot_mua_stats=falcon_clients.plot_live.live_mua_stats:main",
            "live_plot_behavior=falcon_clients.plot_live.live_plot_behavior:main",
            "live_plot_decoding_error=falcon_clients.plot_live.live_plot_decoding_error:main",
            "live_plot_ripple_stats=falcon_clients.plot_live.live_ripple_stats:main",
        ]
    },
    install_requires=[
        "pyzmq",
        "pyyaml",
    ],
    author="Fabian Kloosterman",
    author_email="fabian.kloosterman@nerf.be",
    description="Kloosterman Lab Data Analysis Tools - python visulalization for Falcon",
    long_description = read_file("README.md"),
    license="GPL3",
    zip_safe=False,
    include_package_data=True,
)
